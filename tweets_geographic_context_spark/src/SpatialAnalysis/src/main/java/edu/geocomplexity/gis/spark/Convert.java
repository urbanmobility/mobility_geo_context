package edu.geocomplexity.gis.spark;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;



public class Convert {

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO Auto-generated method stub
        File file = new File("2014_jan_chi_iso.txt");
        BufferedReader reader = null;
        reader = new BufferedReader(new FileReader(file));
        String tempString = null;

        File outputfile = new File("2014_jan_chi_iso_wkt.txt");
        outputfile.createNewFile();
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputfile));


        int i=0;
        while ((tempString = reader.readLine()) != null) {
            System.out.println(tempString);
            String[] strArr = tempString.split(",");
            String wktStr = i+"\t"+strArr[0]+"\t"+strArr[3]+"\t"+"POINT("+strArr[2]+" "+strArr[1]+")";
            bufferedWriter.write(wktStr);
            bufferedWriter.newLine();
            i++;
        }
        reader.close();

        bufferedWriter.close();



    }

}
