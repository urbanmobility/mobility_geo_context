package edu.geocomplexity.gis.spark;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.common.base.Optional;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.rdd.RDD;

import scala.Tuple2;
import scala.Tuple3;

import com.esri.core.geometry.Envelope;
import com.esri.core.geometry.Envelope2D;
import com.esri.core.geometry.Geometry;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polygon;
import com.esri.core.geometry.QuadTree;
import com.esri.core.geometry.QuadTree.QuadTreeIterator;

import edu.geocomplexity.gis.Cell;
import edu.geocomplexity.gis.CubeCoordinate;

public class SparkMain {

    public static double CELLSIZE = 0.05;    //Grid size in cube coordinate
    public static String DECOLLATOR = "\t";  //the decollator among fields in each record

    /**
     * args[0] minx
     * args[1] miny
     * args[2] maxx
     * args[3] maxy
     * args[4] POI file path     hdsf://
     * args[5] area layer file path   hdfs://
     * args[6] distance   0.0005
     * args[7] result file path   hdfs://
     * args[8] minpartition  (Optimal) stands for the minimun tasks for this job
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        if(args.length<8){
            System.out.println("The parameters are: [minx] [minx] [maxx] [maxy] [POI file path] [minx] [distance] [minpartition(Optional)]");
            System.exit(0);
        }

		/*Step1:
		 * Initialize the global extent of area layer,
		 *and the globalExtent is used to build quadtree for each partition
		 */
        double minX = Double.parseDouble(args[0]);
        double minY = Double.parseDouble(args[1]);
        double maxX = Double.parseDouble(args[2]);
        double maxY = Double.parseDouble(args[3]);
        Envelope2D globalExtent = new Envelope2D(minX,minY,maxX,maxY);

        JavaSparkContext sc = new JavaSparkContext();
        //broad cast variables to executors
        //distance globalExtent
        final Broadcast<Double> bDistance = sc.broadcast(Double.parseDouble(args[6]));
        final Broadcast<Envelope2D> bglobalExtent = sc.broadcast(globalExtent);

		/*
		 * Step2:
		 * Read POIs and area layer from HDFS ,each file is divided into at least minPartitions partitions.
		 * Map each object into a grid according to Cube Coordinate
		 * Maybe, some objects may flat map to multiple grids
		 */
        int minPartitions = 10;
        if(args.length==9){
            minPartitions = Integer.parseInt(args[8]);
        }

        //Map the POIs to corresponding Cell
        JavaPairRDD<String, Iterable<Tuple2<String,Point>>> poiLayerRDD =
                sc.textFile(args[4], minPartitions).mapToPair(new PairFunction<String, String, Tuple2<String,Point>>() {
                    public Tuple2<String, Tuple2<String, Point>> call(
                            String arg0) throws Exception {
                        // TODO Auto-generated method stub
                        CubeCoordinate.cellSize = SparkMain.CELLSIZE;

                        String[] fieldsArr = arg0.split(",");
                        Point p = new Point();
                        p.setX(Double.parseDouble(fieldsArr[2]));
                        p.setY(Double.parseDouble(fieldsArr[1]));
                        String attributeFields = fieldsArr[0]+SparkMain.DECOLLATOR+fieldsArr[3];
                        Cell cell = CubeCoordinate.GetPointCell(p,true);
                        Tuple2<String,Point> value = new Tuple2<String,Point>(attributeFields,p);

                        return new Tuple2<String,Tuple2<String,Point>>(cell.toString(),value);
                    }
                }).groupByKey();


        //Map the polygon to corresponding Cells,and the boundary of each polygon is buffered
        JavaPairRDD<String, Iterable<Tuple3<String,Geometry,Geometry>>> areaLayerRDD =
                sc.textFile(args[5], minPartitions).flatMapToPair(new PairFlatMapFunction<String, String, Tuple3<String,Geometry,Geometry>>() {


                    public Iterable<Tuple2<String, Tuple3<String,Geometry,Geometry>>> call(String arg0)
                            throws Exception {
                        // TODO Auto-generated method stub
                        List<Tuple2<String, Tuple3<String,Geometry,Geometry>>> records = new ArrayList<Tuple2<String, Tuple3<String,Geometry,Geometry>>>();

                        CubeCoordinate.cellSize = SparkMain.CELLSIZE;

                        //split each line and get the wkt field, in this method, the wkt field is the last field
                        String[] fieldsArr = arg0.split(SparkMain.DECOLLATOR);
                        String wkt = fieldsArr[fieldsArr.length-1];

                        Geometry geo = GeometryEngine.geometryFromWkt(wkt, 0, Geometry.Type.Polygon);

                        //buffer
                        Polygon bufferedPolygon = GeometryEngine.buffer(geo, null, bDistance.getValue());
                        Envelope env = new Envelope();
                        bufferedPolygon.queryEnvelope(env);

                        List<Cell> cells = CubeCoordinate.GetCellRange(env);
                        for(int i=0;i<cells.size();i++){
                            String key = cells.get(i).toString();
                            Tuple3<String,Geometry,Geometry> value = new Tuple3<String,Geometry,Geometry>(GetAttributeStr(fieldsArr),geo,bufferedPolygon);
                            records.add(new Tuple2<String,Tuple3<String,Geometry,Geometry>>(key,value));
                        }
                        return records;
                    }
                }).groupByKey();

        /*
		 * Join the object with the same cell
		 */
        poiLayerRDD.leftOuterJoin(areaLayerRDD).flatMap(new FlatMapFunction<Tuple2<String,Tuple2<Iterable<Tuple2<String,Point>>,Optional<Iterable<Tuple3<String,Geometry,Geometry>>>>>, String>() {
            public Iterable<String> call(
                    Tuple2<String, Tuple2<Iterable<Tuple2<String, Point>>, Optional<Iterable<Tuple3<String, Geometry, Geometry>>>>> arg0)
                    throws Exception {
                // TODO Auto-generated method stub
                QuadTree quadTree = new QuadTree(bglobalExtent.getValue(), 8);	                                         //Build quad tree for polygon layer
                Map<Integer,Tuple3<String, Geometry, Geometry>> geometryMap = new HashMap<Integer,Tuple3<String, Geometry, Geometry>>();   //Store polygon object in a HashMap, the key is the FID

                List<String> results = new ArrayList<String>();
                if(arg0._2._2.isPresent()){
                    //Build quad tree
                    Iterator<Tuple3<String, Geometry, Geometry>> polygonIterator = arg0._2._2.get().iterator();
                    int index = 0;
                    while(polygonIterator.hasNext()){
                        Tuple3<String, Geometry, Geometry> item = polygonIterator.next();
                        Envelope env = new Envelope();
                        item._3().queryEnvelope(env);

                        quadTree.insert(index, new Envelope2D(env.getXMin(),env.getYMin(),env.getXMax(),env.getYMax()));
                        geometryMap.put(index, item);
                        index++;
                    }
                    QuadTreeIterator iterator = quadTree.getIterator();

                    //Get corresponding polygon for each POI
                    Iterator<Tuple2<String,Point>> poiIterator = arg0._2._1.iterator();
                    while(poiIterator.hasNext()){
                        Tuple2<String,Point> item = poiIterator.next();
                        Point point = item._2();
                        Envelope env = new Envelope();
                        point.queryEnvelope(env);

                        iterator.resetIterator(new Envelope2D(env.getXMin(),env.getYMin(),env.getYMin(),env.getYMax()), 0);
                        int elmHandle = iterator.next();
                        int nearstPolygonId = -1;
                        double distance = Double.MAX_VALUE;
                        boolean isContain = false;

                        //find the nearest polygon
                        while (elmHandle >= 0) {
                            int featureIndex = quadTree.getElement(elmHandle);
                            Geometry bufferedPolygon = geometryMap.get(featureIndex)._3();
                            if(GeometryEngine.contains(bufferedPolygon,point,  null)){
                                isContain = true;
                                double tempdistance = GeometryEngine.distance(geometryMap.get(featureIndex)._2(), point, null);
                                if(tempdistance<distance){
                                    nearstPolygonId = featureIndex;
                                    distance = tempdistance;
                                }
                            }

                            elmHandle = iterator.next();
                        }
                        if(isContain){
                            //do some thing
                            String[] arr = geometryMap.get(nearstPolygonId)._1().split(SparkMain.DECOLLATOR);
                            results.add(item._1+SparkMain.DECOLLATOR+arr[1]+SparkMain.DECOLLATOR+GeometryEngine.geometryToWkt(point,0));
                        }else{
                            results.add(item._1+SparkMain.DECOLLATOR+"9999"+SparkMain.DECOLLATOR+GeometryEngine.geometryToWkt(point,0));
                        }
                    }

                }else{
                    Iterator<Tuple2<String,Point>> poiIterator = arg0._2._1.iterator();
                    while(poiIterator.hasNext())
                    {
                        Tuple2<String,Point> item = poiIterator.next();
                        Point point = item._2();
                        results.add(item._1+SparkMain.DECOLLATOR+"9999"+SparkMain.DECOLLATOR+GeometryEngine.geometryToWkt(point,0));
                    }
                }

                return results;
            }
        }).saveAsTextFile(args[7]);

    }

    public static String GetAttributeStr(String[] arr){
        StringBuffer sb = new StringBuffer();
        for(int i=0;i<arr.length-1;i++){
            sb.append(arr[i]+SparkMain.DECOLLATOR);
        }
        return sb.toString();
    }


}
